import { addWeeks, format, isAfter, isBefore } from 'date-fns'

const getRecord = (row) => {
  return {
    dateReported: new Date(row[8]),
    country: row[9],
    weekNum: row[10],
    dateWeekEnding: format(new Date(row[11]), 'MM/dd/yy'),
    date: new Date(row[11]),
    sex: row[12],
    age: row[13],
    totalDeaths: parseInt(row[14]),
    covidDeaths: parseInt(row[15])
  }
}

const colors = [
  '#eec643',
  '#141414',
  '#eef0f2',
  '#0d21a1',
  '#011638',
  '#6dd3ce',
  '#c8e9a0',
  '#e4b7e5',
  '#f7a278',
  '#a13d63',
  '#351e29',
  '#52aa5e',
  '#9a48d0'
]

const getMonthYear = (date) => {
  return format(date, 'MMM-yy')
}

export default (data, { ingoreEarlyWeeks, ignoreRecentWeeks, groupByMonth = false, separateGender = false, allAges = false }) => {
  const labels = []
  const sets = {}

  // start with total deaths, by age group per week ending
  let colorIndex = 0
  let dateReported
  const filtered = data.data.filter(row => {
    const record = getRecord(row)
    dateReported = format(record.dateReported, 'MM/dd/yyyy')
    if (ingoreEarlyWeeks) {
      if (isBefore(record.date, new Date('03/10/2020'))) {
        return false
      }
    }

    if (ignoreRecentWeeks) { // exclude 3 recent weeks
      if (isAfter(record.date, addWeeks(new Date(), -3))) {
        return false
      }
    }

    return record.sex === 'All Sex' // && record.age !== 'All Ages'
  })

  // const lastMonth = ''

  let totalDeaths = 0

  filtered.forEach(row => {
    const record = getRecord(row)
    const currentMonth = getMonthYear(record.date)

    if (!labels.includes(groupByMonth ? currentMonth : record.dateWeekEnding)) {
      labels.push(groupByMonth ? currentMonth : record.dateWeekEnding)
    }

    if (!sets[record.age]) {
      sets[record.age] = {
        tmp: {},
        data: [],
        label: record.age,
        borderColor: colors[colorIndex],
        backgroundColor: colors[colorIndex],
        fill: false
      }
      colorIndex++
    }

    if (groupByMonth) {
      if (!sets[record.age].tmp[currentMonth]) {
        sets[record.age].tmp[currentMonth] = 0
      }

      sets[record.age].tmp[currentMonth] = sets[record.age].tmp[currentMonth] + record.covidDeaths
    } else {
      sets[record.age].data.push(record.covidDeaths)
    }
    if (record.age !== 'All Ages') {
      totalDeaths += record.covidDeaths
    }
  })

  if (groupByMonth) {
    labels.forEach((label) => {
      // for each set
      Object.keys(sets).forEach((setKey) => {
        sets[setKey].data.push(sets[setKey].tmp[label])
      })
    })
  }

  const datasets = []
  Object.keys(sets).forEach(datasetkey => datasets.push(sets[datasetkey]))

  return {
    totalDeaths,
    dateReported,
    labels,
    datasets
  }
}

/**
 * {
    labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
    datasets: [{
        data: [86,114,106,106,107,111,133,221,783,2478],
        label: "Africa",
        borderColor: "#3e95cd",
        fill: false
      }, {
        data: [282,350,411,502,635,809,947,1402,3700,5267],
        label: "Asia",
        borderColor: "#8e5ea2",
        fill: false
      }, {
        data: [168,170,178,190,203,276,408,547,675,734],
        label: "Europe",
        borderColor: "#3cba9f",
        fill: false
      }, {
        data: [40,20,10,16,24,38,74,167,508,784],
        label: "Latin America",
        borderColor: "#e8c3b9",
        fill: false
      }, {
        data: [6,3,2,2,7,26,82,172,312,433],
        label: "North America",
        borderColor: "#c45850",
        fill: false
      }
    ]
  },
 */
