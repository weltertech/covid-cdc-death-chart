import React, { useEffect, useState } from 'react'
import axios from 'axios'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import Tooltip from '@material-ui/core/Tooltip'
import Button from '@material-ui/core/Button'

import './App.css'
// import rawData from './data.json'
import parseData from './parseData'

import { Line } from 'react-chartjs-2'

const inputStyle = {
  width: '300px',
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'flex-start',
  justifyContent: 'space-between'
}

function App () {
  const [data, setData] = useState()
  const [isLoading, setIsLoading] = useState(true);
  const [ignoreRecentWeeks, setIgnoreRecentWeeks] = useState(true)
  const [ingoreEarlyWeeks, setIgnoreEarlyWeeks] = useState(true)
  const [groupByMonth, setGroupByMonth] = useState(false)

  useEffect(() => {
    const loadData = async () => {
      const response = await axios.get('https://data.cdc.gov/api/views/vsak-wrfu/rows.json?accessType=DOWNLOAD')
      const parsedData = parseData(response.data, { ignoreRecentWeeks, ingoreEarlyWeeks, groupByMonth })
      setData(parsedData)
      setIsLoading(false)
    }

    loadData()
  }, [ignoreRecentWeeks, ingoreEarlyWeeks, groupByMonth, setIsLoading])

  if (isLoading) {
    return <div>Loading data...wear a mask, wash your hands, don't touch your face.</div>
  }

  return (
    <div className='App'>
      <h1>Weekly Covid Deaths by Age Group</h1>
      <h2>Total Deaths: {data.totalDeaths}</h2>
      <h3>Data sourced from <a href='https://data.cdc.gov/NCHS/Provisional-COVID-19-Death-Counts-by-Sex-Age-and-W/vsak-wrfu/data'>CDC</a>, last updated: {data && data.dateReported}</h3>
      {data && <Line data={data} />}

      <FormGroup row style={{ display: 'flex', alignItems: 'center', flexDirection: 'column', margin: '50px' }}>
        <div style={inputStyle}>
          <FormControlLabel
            control={<Checkbox checked={ignoreRecentWeeks} onChange={(e) => setIgnoreRecentWeeks(e.target.checked)} name='checkedA' />}
            label='Ignore Recent Weeks'
          />
          <Tooltip title='States can take multiple weeks to report data so latest data is incomplete' interactive>
            <Button>?</Button>
          </Tooltip>
        </div>
        <div style={inputStyle}>
          <FormControlLabel
            control={<Checkbox checked={ingoreEarlyWeeks} onChange={(e) => setIgnoreEarlyWeeks(e.target.checked)} name='checkedA' />}
            label='Ignore Early Weeks'
          />
          <Tooltip title='The dataset contains weeks before mid-march with 0 deaths' interactive>
            <Button>?</Button>
          </Tooltip>
        </div>
        <div style={inputStyle}>
          <FormControlLabel
            control={<Checkbox checked={groupByMonth} onChange={(e) => setGroupByMonth(e.target.checked)} name='checkedA' />}
            label='Group By Month'
          />
          <Tooltip title='Display deaths grouped by month rather than week' interactive>
            <Button>?</Button>
          </Tooltip>
        </div>
      </FormGroup>
    </div>
  )
}

export default App

/**
 * Next dataset:
 * https://data.cdc.gov/Case-Surveillance/United-States-COVID-19-Cases-and-Deaths-by-State-o/9mfq-cb36
 */
